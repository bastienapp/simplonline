public interface Authentication {

    void login(String email);

    default void logout() {

        System.out.println("Vous êtes déconnecté");
    }
}
