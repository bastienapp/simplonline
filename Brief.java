import java.util.Date;

public class Brief {

  // attributs (privé par convention)
  private String title;
  private String content;
  private Date creationDate;
  private Instructor author;

  // si y'a pas de constructeur, y'a un constructeur vide par défaut

  // surcharge de constructeur

  public Brief() {

  }

  public Brief(
    String titleValue
  ) {
    this.title = titleValue;
  }

  public Brief(
    String titleValue,
    String contentValue,
    Instructor instructorValue
  ) {
    this.title = titleValue;
    this.content = contentValue;
    this.author = instructorValue;
  }

  // méthodes

  // accesseurs / mutateurs
  // getter
  public String getTitle() {
    return this.title.toUpperCase();
  }
  // setter
  public void setTitle(String newTitle) {
    this.title = newTitle;
  }
}
