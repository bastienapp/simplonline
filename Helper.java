public class Helper {

    public static int add(int a, int b) {
        return a + b;
    }

    public static double add(double a, double b) {
        return a + b;
    }

    public static String formatDoubleToEuro(double value) {
        return Double.toString(value) + " €";
    }
}
