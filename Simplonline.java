import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

class Simplonline {

  public static void main(String[] args) {

    // surcharge de méthode
    var res1 = Helper.add(1, 2);
    var res2 = Helper.add(1, 2.5);

    // type primitif
    int a = 3;
    int b = a; // j'ai copié la valeur : parce que c'est un type primitif
    a++;
    System.out.println(a + " " + b);

    Instructor bastien = new Instructor("ZDZ6546845", "bastien@simplon.co", "Bastien", "Krettly", 35, 100);
    // type objet (Brief)
    Brief brief1 = new Brief("Brief 1 - Java", "exemple de brief", bastien); // créer une instance de Brief
    Brief brief2 = brief1; // copié la référence : parce que c'est un objet

    System.out.println(brief1 + " " + brief2); // "Brief@7ad041f3"
    brief1.setTitle("Java - Simplonline");
    System.out.println(brief1.getTitle());

    var fullname = bastien.fullname();
    // bastien.workedHours = 40;
    System.out.println(fullname + " " + bastien.salary());

    CampusManager nicolas = new CampusManager(
        "IUHIUH564",
        "nico@simplon.co",
        "Nicolas",
        "Simplonline",
        new String[] { "Labège", "Bellefontaine" });
    // System.out.println(nicolas.fullname());

    Staff[] employeeList = { bastien, nicolas };
    for (Staff employee : employeeList) {
      System.out.println(employee.fullname() + " " + employee.hasRole());
      // System.out.println(employee.getCampuses());
    }

    ArrayList<Person> personList = new ArrayList<>();
    personList.add(nicolas);
    personList.add(bastien);

    Student michel = new Student("michel@simplon.co", "Michel", "Durand", "Java");
    LinkedList<Brief> briefMichel = new LinkedList<>();
    briefMichel.add(brief1);
    michel.setBriefList(briefMichel);
  }
}
