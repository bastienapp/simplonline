import java.util.List;

public class Student extends Person implements Authentication {

    String training;
    List<Brief> briefList; // on peut ajouter/retirer des éléments dans une liste, pas de limite
    // List vs ArrayList

    public Student(String email, String firstName, String lastName, String training) {
        super(email, firstName, lastName);
        this.training = training;
    }

    public void assignBrief(Brief brief) {
        briefList.add(0, brief);
    }

    public void setBriefList(List<Brief> briefList) {
        this.briefList = briefList;
    }

    @Override
    public void login(String email) {
        System.out.println("L'apprenent⋅e c'est connecté⋅e avec l'email " + this.getEmail());
    }
}
