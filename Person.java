public abstract class Person {

    private String email;
    private String firstName;
    private String lastName;

    public Person(String email, String firstName, String lastName) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String fullname() {
        return this.firstName + " " + this.lastName;
    }

    // getter / setter
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName.toUpperCase();
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
