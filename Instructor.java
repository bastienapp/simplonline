public class Instructor extends Staff implements Authentication{

    private int workedHours;
    private int hourlyRate;

    public Instructor(String employeeId, String email, String firstName, String lastName, int workedHours, int hourlyRate) {
        super(employeeId, email, firstName, lastName);

        this.workedHours = workedHours;
        this.hourlyRate = hourlyRate;
    }

    public String salary() {
        if (this.workedHours > 35) {
            var overtime = this.workedHours - 35;
            return Helper.formatDoubleToEuro(35 * this.hourlyRate + overtime * this.hourlyRate * 1.5);
        }
        return Helper.formatDoubleToEuro(this.workedHours * this.hourlyRate);
    }

    // redéfinition
    @Override
    public String hasRole() {
        return "Instructor";
    }

    @Override
    public void login(String email) {
        System.out.println("Le⋅la formateur⋅rice c'est connecté⋅e avec l'email " + this.getEmail());
    }

    @Override
    public void logout() {
        System.out.println("Le formateur c'est déconnecté");
    }
}
