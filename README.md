
```bash
javac Simplonline.java
```
> point d'entrée
  Simplonline

Brief

Staff
  Instructor
  CampusManager


Collection : nombre d'élément, taille de la collection, ajouter, supprimer

Liste : une collection ou les élément sont liés entre eux
  ArrayList : chaque élément aura un index associé
  LinkedList : pas d'index mais les élément restent liés les un autre autre
  Vector ???

Ensemble (Set) : les élements ne sont pas liés entre eux
  - HashSet : tiroir fourre tout, pas de duplication possible
  - LinkedHashSet ???

Queue : les élément se suivent, on n'a accès qu'au premier et au dernier
  - PriorityQueue

