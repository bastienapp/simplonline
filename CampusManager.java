public class CampusManager extends Staff {

    // extension de la classe parente
    private String[] campuses;

    public CampusManager(String employeeId, String email, String firstName, String lastName, String[] campuses) {
        super(employeeId, email, firstName, lastName);
        this.campuses = campuses;
    }

    public String[] getCampuses() {
        return this.campuses;
    }

    // redéfinition
    @Override
    public String hasRole() {
        return "Campus Manager";
    }
}
