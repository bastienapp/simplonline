// POJO : attributs privés, constructeur, getter / setter
public class Staff extends Person {

  private String employeeId;

  public Staff(String employeeId, String email, String firstName, String lastName) {
    super(email, firstName, lastName);
    this.employeeId = employeeId;
  }

  public String hasRole() {
    return "Staff";
  }

  public static String staticTest() {
    return "This is static";
  }
}
